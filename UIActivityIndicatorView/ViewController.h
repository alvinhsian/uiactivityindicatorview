//
//  ViewController.h
//  UIActivityIndicatorView
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/4/1.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *myIndicator;

- (IBAction)toClick:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *toClickText;
@end

