//
//  ViewController.m
//  UIActivityIndicatorView
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/4/1.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [_myIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toClick:(id)sender {
    
    //判斷動畫是否正在轉動
    if (_myIndicator.isAnimating==YES) {
        
        //停止轉動行為並消失
        [_myIndicator stopAnimating];
       
        //關閉動畫之後設定按鈕文字為Start
        [_toClickText setTitle:@"Start" forState:UIControlStateNormal];
        
    } else {
        //執行轉動行為並顯示
        [_myIndicator startAnimating];
        
        //開啟動畫之後設定按鈕文字為Stop
        [_toClickText setTitle:@"Stop" forState:UIControlStateNormal];
    }
}
@end
